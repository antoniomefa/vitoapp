import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import Login from './src/containers/login/index';
import RecoverPass from './src/containers/login/recoverpass';
import Home from './src/containers/home/home';

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator(
  {
    Login: Login,
    Recover: RecoverPass,
    Home: Home
  },
  {
    initialRouteName: "Login"
  }
);

const AppContainer = createAppContainer(AppNavigator);