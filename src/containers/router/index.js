import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import routes from 'utils/routes';
//import Wrapper from 'containers/wrapper';
//import { userIsAuthenticated, userIsntAuthenticated } from 'utils/middleware';
import { Router, Route, Redirect, IndexRoute } from 'react-router';
import { Switch } from 'react-router-dom';
//import { history } from 'utils/store';
import moment from 'moment';
import { sync } from 'actions';
import { getSession, getAccessToken, getPhorter } from 'helpers';

class RouterContainer extends Component {

    componentDidMount() {
        const session = getSession();
        const accessToken = getAccessToken();
        const phorther = getPhorter();

        if (session && accessToken && phorther) {
            sync();
        }

        moment.locale('es');
    }

    getComponent = route => {
        const Component = route.component;

        if (route.authorizationRequired) return (
            userIsAuthenticated(props =>
                <Wrapper { ...props } route={route} >
                    <Component />
                </Wrapper>
            )
        );

        return userIsntAuthenticated((props) => (
            <Wrapper route={route} { ...props }>
                <Component />
            </Wrapper>
        ));
    }

    render() {
        return (
            <NativeRouter history={history}>
                <Switch>
                    {
                        routes && routes.map((route, index) => (
                            <Route
                                key={index}
                                path={route.path}
                            >
                                <IndexRoute component={this.getComponent(route)} />
                                {
                                    route.children && route.children.map((child, childIndex) => (
                                        <Route
                                            key={`child-${childIndex}`}
                                            path={child.path}
                                            component={this.getComponent(child)}
                                        />
                                    ))
                                }
                            </Route>
                        ))
                    }
                    <Redirect from='*' to='/not-found' />
                </Switch>
            </NativeRouter>
        );
    }
}

// Router.propTypes = {
//
// };

Router.defaultOptions = {

};

export default RouterContainer;
