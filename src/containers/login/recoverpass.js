import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import { connect } from 'react-redux';
import { auth } from './actions';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ImageBackground,
    Dimensions,
    TextInput,
    TouchableOpacity,
    ToastAndroid
  } from 'react-native';
  
  import bgImage from '../../assets/background.jpg';
  import logoImg from '../../assets/logo.png';
  
  const { width: WIDTH } = Dimensions.get('window');

  class RecoverPass extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            text: 'Recuperar contraseña',
            loading: false,
            requested: false,
            showPass: true,
            press: false,
            username: '',
            password: ''
        };
    }

    handleClic() {
       if(!this.allFieldsRequiredFilled()) {
           ToastAndroid.show('Ingrese su correo electrónico', ToastAndroid.LONG);
           return;
       }
       this.checkRegex();
    }

    allFieldsRequiredFilled = () => [this.state.username].every(f => f != '');

    checkRegex = () => {
        /* eslint-disable */
        var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        /* eslint-enable */
        if (!emailRegex.test(this.state.username)) {
            ToastAndroid.show('No es un correo válido', ToastAndroid.LONG);
            return;
        }
        
        this.setState({
            requested: true,
            loading: true,
            text: 'Enviando...'
            }, () => {
                auth(this.state.username, this.state.password);
        });
    }

    render() {
        return (
            <ImageBackground
              source={bgImage}
              style={styles.backgroundContainer}>

                <View style={styles.logoContainer}>
                    <Text style={styles.welcomeText}>{this.state.text}</Text>
                    <Image source={logoImg} style={styles.logo}/>
                    <Text style={styles.logoText}>Ingresa el correo electrónico que registraste en Vito y te enviaremos las indicaciones para restablecer tu contraseña.</Text>
                </View>

                <View style={styles.inputContainer}>
                    <Text style={styles.inputIcon}>👤</Text>
                    <TextInput
                        style={styles.input}
                        placeholder={'Correo electrónico'}
                        onChangeText={(username) => this.setState({username})}
                        value={this.state.username}
                        placeholderTextColor={'rgba(255,255,255, 0.7)'}
                        underlineColorAndroid='transparent'
                    />
                </View>

                <TouchableOpacity style={styles.btnLogin}
                    onPress={this.handleClic.bind(this)}>
                    <Text style={styles.text}>Enviar correo</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.forgotPass}
                   onPress={() => this.props.navigation.navigate('Login')}
                >
                    <Text style={styles.forgotPassText}>Regresar a inicio de sesión</Text>
                </TouchableOpacity>
                 
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create ({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoContainer: {
      alignItems: 'center',
      marginBottom: 50
  },
  welcomeText: {
      color: 'white',
      fontSize: 40,
      fontWeight: '500',
      opacity: 0.6,
      marginBottom: 20,
  },
  logo: {
      width: 300,
      height: 120
  },
  logoText: {
      color: 'white',
      fontSize: 20,
      fontWeight: '500',
      justifyContent: 'center',
      opacity: 0.8,
      marginTop: 15,
      width: WIDTH - 70
  },
  inputContainer: {
      marginBottom: 10
  },
  input: {
      width: WIDTH - 55,
      height: 45,
      borderRadius: 25,
      fontSize: 16,
      paddingLeft: 45,
      backgroundColor: 'rgba(0,0,0, 0.35)',
      color: 'rgba(255,255,255, 0.7)',
      marginHorizontal: 25
  },
  inputIcon: {
      position: 'absolute',
      top: 12,
      left: 38
  },
  btnLogin: {
      width: WIDTH - 55,
      height: 45,
      borderRadius: 25,
      backgroundColor: 'rgba(50,200,50, 0.9)',
      marginTop: 10
  },
  text: {
      color: 'rgba(255,255,255, 0.7)',
      fontSize: 28,
      textAlign: 'center'
  },
  forgotPass: {
      marginTop: 40
  },
  forgotPassText: {
    color: 'rgba(255,255,255, 0.9)',
      fontSize: 16,
      textAlign: 'center'
}
});

//Login.propTypes = {
//     showAlert: PropTypes.func,
//     sendNotification: PropTypes.func,
//     goTo: PropTypes.func,
//     goToab: PropTypes.func,
//  auth: PropTypes.func
//     initForm: PropTypes.func,
//     showNotice: PropTypes.func
//};

// Login.defaultProps = {
//     auth: () => {}
// };

// const mapStateToProps = ({ login }) => ({
//     ...login
// });

 const mapDispatchToProps = {
//     showAlert,
//     sendNotification,
//     goTo,
//     goToab,
//     showNotice,
//     initForm,
//    auth
};

//export default connect(mapStateToProps, mapDispatchToProps)(Login);
export default RecoverPass;