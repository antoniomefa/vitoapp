import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ImageBackground,
    Dimensions,
    TextInput,
    TouchableOpacity,
    ToastAndroid
  } from 'react-native';
import PromoImg from '../../assets/invitar-bg.jpg';

const { width: WIDTH } = Dimensions.get('window');

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.promoText}>INVITA A TUS AMIGOS A CUIDAR DE SÍ</Text>
                <Image source={PromoImg} style={styles.promoImg} />
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center'
    },
    promoText: {
        color: 'black',
        fontSize: 16
    },
    promoImg: {
        width: WIDTH - 80,
        height: 200
    }
})

export default Home;