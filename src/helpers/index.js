// import store from 'utils/store';
// import { goSignIn } from 'redux-actions/auth';
// import { ON_RENEW_SESSION } from 'action-types/auth';
//import JSZip from 'jszip';
//import { saveAs } from 'file-saver/FileSaver';
import AsyncStorage from '@react-native-community/async-storage';
import base64 from 'react-native-base64';


export const createSession = async (payload) => {
    const data = base64.encode(JSON.stringify(payload));
    try {
        await AsyncStorage.setItem('__b_m_usr_d', data);
    } catch (error) {
        console.log(error.message); // Error saving data
    }
};

export const createAccessToken = async (payload) => {
    const data = base64.encode(JSON.stringify(payload));
    try {
        await AsyncStorage.setItem('__b_m_usr_ae', data);
    } catch (error) {
        console.log(error.message); // Error saving data
    }
};

export const createPhorter = async (payload) => {
    const data = base64.encode(JSON.stringify(payload));
    try {
        await AsyncStorage.setItem('__b_m_usr_ph', data);
    } catch (error) {
        console.log(error.message); // Error saving data
    }
};

export const getAccessToken = async () => { 
    try {
        const data = await AsyncStorage.getItem('__b_m_usr_ae');
        if (!data) return null;
        return JSON.parse(base64.decode(data));
    } catch (error) {
        console.log(error.message);
    }
};

export const getSession = async () => {
    try {
        const data = await AsyncStorage.getItem('__b_m_usr_d');
        if (!data) return null;
        return JSON.parse(base64.decode(data));
    } catch (error) {
        console.log(error.message);
    }
};

export const getPhorter = async () => { /// Toño:  Es una cadena de números que contiene los permisos de cada usuario
    try {
        const data = await AsyncStorage.getItem('__b_m_usr_ph');
        if (!data) return null;
        return JSON.parse(base64.decode(data));
    } catch (error) {
        console.log(error.message);
    }
};

export const removePhorter = async () => {
    try {
        await AsyncStorage.removeItem('__b_m_usr_ph');
    } catch (error) {
        console.log(error.message); // Error retrieving data
    }
};

export const removeSession = async () => {
    try {
        await AsyncStorage.removeItem('__b_m_usr_d');
    } catch (error) {
        console.log(error.message); // Error retrieving data
    }
};

export const removeAccessToken = async () => {
    try {
        await AsyncStorage.removeItem('__b_m_usr_ae');
    } catch (error) {
        console.log(error.message); // Error retrieving data
    }
};

export const saveAccess = (accessToken, phorther) => {
    createAccessToken(accessToken);
    createPhorter(phorther);
};

export const logout = () => {
    removePhorter();
    removeSession();
    removeAccessToken();
    location.reload(true); // clear cache
};

// export const downloadB64 = (mime, name, base64) => {
//     const ZERO = 0;
//     const NOT_FOUND = -1;
//     var contentType = `image/${mime}`;
//     var sliceSize = 1024;
//     var byteCharacters = atob(base64);
//     var bytesLength = byteCharacters.length;
//     var slicesCount = Math.ceil(bytesLength / sliceSize);
//     var byteArrays = new Array(slicesCount);
//     var sliceIndex = ZERO;
//     var begin, end, bytes, offset, i, blob, match;

//     if (mime === 'pdf') contentType = 'application/pdf';
//     else if (mime === 'xml') contentType = 'text/xml';

//     for (sliceIndex = ZERO; sliceIndex < slicesCount; ++sliceIndex) {
//         begin = sliceIndex * sliceSize;
//         end = Math.min(begin + sliceSize, bytesLength);

//         bytes = new Array(end - begin);
//         for (offset = begin, i = ZERO; offset < end; ++i, ++offset) {
//             bytes[i] = byteCharacters[offset].charCodeAt(ZERO);
//         }
//         byteArrays[sliceIndex] = new Uint8Array(bytes);
//     }

//     blob = new Blob(byteArrays, {
//         type: contentType
//     });

//     match = navigator.userAgent.search(/(?:Edge|MSIE|Trident\/.*; rv:)/);
//     if (match !== NOT_FOUND) {
//         if (window.navigator && window.navigator.msSaveOrOpenBlob) {
//             window.navigator.msSaveOrOpenBlob(blob, name);
//         }
//     } else {

//     const a = document.createElement('a');
//     document.body.appendChild(a);
//     a.style = 'display: none';

//     const url = window.URL.createObjectURL(blob);
//     a.href = url;
//     a.download = name;
//     a.click();
//     window.URL.revokeObjectURL(url);
//   }

// };

// export const downloadB64AsZIP = (array, zipName) => {
//     // array => mime, name, base64
//     const zipArchive = new JSZip();
//     const memoryName = zipName;
//     const zip = zipArchive.folder(memoryName);
//     const ZERO = 0;
//     const NOT_FOUND = -1;

//     array.forEach(({ mime, name, data }) => {
//         var contentType = `image/${mime}`;
//         var sliceSize = 1024;
//         var byteCharacters = atob(data);
//         var bytesLength = byteCharacters.length;
//         var slicesCount = Math.ceil(bytesLength / sliceSize);
//         var byteArrays = new Array(slicesCount);
//         var sliceIndex = ZERO;
//         var begin, end, bytes, offset, i, blob, match;

//         if (mime === 'pdf') contentType = 'application/pdf';
//         else if (mime === 'xml') contentType = 'text/xml';

//         for (sliceIndex = ZERO; sliceIndex < slicesCount; ++sliceIndex) {
//             begin = sliceIndex * sliceSize;
//             end = Math.min(begin + sliceSize, bytesLength);

//             bytes = new Array(end - begin);
//             for (offset = begin, i = ZERO; offset < end; ++i, ++offset) {
//                 bytes[i] = byteCharacters[offset].charCodeAt(ZERO);
//             }
//             byteArrays[sliceIndex] = new Uint8Array(bytes);
//         }

//         blob = new Blob(byteArrays, {
//             type: contentType
//         });

//         match = navigator.userAgent.search(/(?:Edge|MSIE|Trident\/.*; rv:)/);
//         if (match !== NOT_FOUND) {
//             if (window.navigator && window.navigator.msSaveOrOpenBlob) {
//                 window.navigator.msSaveOrOpenBlob(blob, name);
//             }
//         } else {
//             zip.file(name, blob);
//         }
//     });

//     zipArchive.generateAsync({type:'blob'}).then(function(content) {
//         // see FileSaver.js
//         saveAs(content, `${memoryName}.zip`);
//     });
// };
