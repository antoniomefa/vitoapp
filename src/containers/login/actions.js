// import { ON_LOGIN_SUCCESS } from 'action-types/auth';
// import { ON_RESET_FORM, ON_LOGIN_FAILED } from './types';
// import { routerActions, push } from 'react-router-redux';
import Service from '../../helpers/service';
import { createSession } from '../../helpers/index';
import { ToastAndroid } from 'react-native';
import base64 from 'react-native-base64';

// export function goTo() {
//     return dispatch => {
//         dispatch(routerActions.push('/sign-up'));
//     };
// }

// export function goToab() {
//     return dispatch => {
//         dispatch(push('/about-us'));
//     };
// }

export const auth = (username, password) => {
    const data = JSON.stringify({ username, password: base64.encode(password) });
    Service(data, 'auth/login', 'post')
        .then(data => {
            createSession(data);
            ToastAndroid.show('Acceso concedido!!!', ToastAndroid.LONG);
        })
        .catch(error => {
            ToastAndroid.show(error, ToastAndroid.LONG);
        });
};

// export const auth = (username, password) =>  dispatch => {
//     const data = JSON.stringify({ username, password: btoa(password) });
//     debugger;
//     Service(data, 'auth/login', 'post')
//         .then(data => {
//             createSession(data);
//             //dispatch({ type: ON_LOGIN_SUCCESS, payload: data });
//             ToastAndroid.show('Acceso correcto!!!', ToastAndroid.LONG);
//         })
//         .catch(error => {
//             ToastAndroid.show('Acceso denegado!!!', ToastAndroid.LONG);
//             // dispatch({
//             //     type: ON_LOGIN_FAILED,
//             //     hasErrorText: error || 'Error inesperado del sistema'
//             // });
//         });
// };

//export const initForm = () => ({ type: ON_RESET_FORM });
